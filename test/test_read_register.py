import base
from pynovus.manager import NovusTcp
from pynovus.registers import Novus1200


def test_get_software_version():
    client = NovusTcp(host="192.168.10.123", port=1000)
    client.connect()
    response = client.read_holding_registers(address=Novus1200.SOFTWARE_VERSION, count=1, slave=1)
    assert not response.isError()
