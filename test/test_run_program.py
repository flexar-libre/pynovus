import base
from pynovus.manager import NovusTcp
from pynovus.registers import Novus1200


def test_run_program():
    client = NovusTcp(host="192.168.10.123", port=1000)
    client.connect()
    response = client.write_register(address=Novus1200.RUN, value=1, slave=1)
    assert not response.isError()
    response = client.write_register(address=Novus1200.PRN, value=1, slave=1)
    assert not response.isError()
    response = client.read_holding_registers(address=Novus1200.RUN, count=1, slave=1)
    assert not response.isError()
    assert response.registers[0] == 1


def test_stop_program():
    client = NovusTcp(host="192.168.10.123", port=1000)
    client.connect()
    response = client.write_register(address=Novus1200.RUN, value=0, slave=1)
    assert not response.isError()
    response = client.read_holding_registers(address=Novus1200.RUN, count=1, slave=1)
    assert not response.isError()
    assert response.registers[0] == 0
