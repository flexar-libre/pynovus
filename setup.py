"""Setup."""
import os
import io
from setuptools import setup


def read(fname):
    """Read file in proyect dir."""
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8'
    ).read()


requires = [
    'pyserial',
    'pymodbus',
]

setup(
    name='pynovus',
    version='0.0.1',
    zip_safe=False,
    author='Flexar S.R.L.',
    author_email='sistemas@flexar.com.ar',
    description='Novus Python connector',
    long_description=read('README.md'),
    platforms='Posix; MacOS X; Windows',
    packages=['pynovus'],
    keywords='flexar novus pid',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Topic :: Internet'
    ],
    python_requires='>=3.8',
    license='GPL-3',
    install_requires=requires,
)
