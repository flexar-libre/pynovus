###################
Guía de instalación
###################

Sobre el módulo
===============

El módulo conector, se comunica con la controladora con abriendo un socket TCP/IP, o puerto serie contra ella. Luego, realiza las diferentes acciones enviando comandos de modbus.

Requisitos
==========

El módulo trabaja en su mayoría sin librerías externas, sin embargo, para el envío y manipulación de datos, se requiere de ``pyserial``.

Instalación normal
==================

Si desea instalar el paquete para utilizarlo lo antes posible. Es tan simple como ejecutar:

.. code-block:: bash

    pip install .

o también:

.. code-block:: bash

    python setup.py install

Esto instalará el módulo y sus dependencias automáticamente.

Instalación en modo desarrollo
==============================

Si desea cambiar el código del módulo para adaptarlo a sus necesidades, es recomendable instalarlo de la siguiente manera:

.. code-block:: bash

    pip install e .

o también:

.. code-block:: bash

    python setup.py develop

Con esto se tiene el mismo efecto que en una *instalación normal*, sin embargo la carpeta del proyecto se *enlazará* a la ruta de paquetes de Python, con lo cual, los cambios hechos en el código se reflejarán automáticamente en la instalación.
