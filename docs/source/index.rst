#######
Pynovus
#######

Este sitio contiene la documentación del proyecto ``pynovus`` que es un conector escrito en *python* para hablar con los controladores `Novus <https://www.novusautomation.com/site/>`_.

Historia
========

Este proyecto nace de la necesidad de automatizar el proceso de curado de productos en una linea de producción industrial.

En la empresa, existen interfaces de usuario para operarios que fueron construidas utilizando tecnologías web. Esta infraestructura de control, trabaja con una *API* escrita en *Python* que se encarga de procesar todas las solicitudes por parte del frontend. Es por esto que surgió la necesidad de controlar el proceso del manejo de hornos, con lo cual, un conector para hablar contra estos controladores y poder administrarlas a distancia fué necesario.

Contenido
=========

La documentación esta divida de la siguiente manera:

.. toctree::
    :caption: Guía de inicio
    :numbered:

    how-to-install
    how-to-guides

Encuentre rápidamente lo que está buscando dependiendo de su caso de uso mirando las diferentes páginas.

.. toctree::
    :caption: API
    :hidden:

    pynovus
