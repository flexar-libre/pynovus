"""Pynovus."""
import typing
from pymodbus.constants import Defaults
from pymodbus.client import ModbusTcpClient, ModbusSerialClient
from pymodbus.transaction import ModbusRtuFramer


class NovusSerial(ModbusSerialClient):
    """Clase de conexión por serie a un controlador Novus."""

    def __init__(
        self, port: str, baudrate=Defaults.Baudrate, bytesize=Defaults.Bytesize,
        parity=Defaults.Parity, stopbits=Defaults.Stopbits,
        handle_local_echo=Defaults.HandleLocalEcho, **kwargs
    ) -> None:
        """Constructor de la clase.

        Examples:
            >>> with NovusSerial() as novus:
            >>>     novus.connect()
        """
        super().__init__(
            port, ModbusRtuFramer, baudrate, bytesize, parity, stopbits, handle_local_echo,
            **kwargs
        )


class NovusTcp(ModbusTcpClient):
    """Clase de conexión por serie a un controlador Novus."""

    def __init__(
        self, host: str, port=Defaults.TcpPort,
        source_address: typing.Tuple[str, int] = None, **kwargs
    ) -> None:
        """Constructor de la clase.

        Examples:
            >>> with NovusTcp() as novus:
            >>>     novus.connect()
        """
        super().__init__(host, port, ModbusRtuFramer, source_address, **kwargs)
