"""Registros de controlador."""
from enum import Enum


class NovusRegisterBase(Enum):
    """Enúmeración para los registros del controlador."""


class Novus1100(NovusRegisterBase):
    """Enúmeración para controlador Novus 1100."""


class Novus2000(NovusRegisterBase):
    """Enúmeración para controlador Novus 2000."""


class Novus1200(NovusRegisterBase):
    """Enúmeración para controlador Novus 1200."""

    ACTIVE_SP = 0
    """Control del setpoint."""
    PV = 1
    """Variable de proceso."""
    MV = 2
    """Potencia de salida en modos automático o manual."""
    REMOTE_SP_TYPE = 3
    """Tipo de input seleccionado para SP remoto."""
    DISPLAY_VALUE = 4
    """Valor actual mostrado en el display."""
    PROMPT_INDEX = 5
    """Posición actual del indicador en el diagrama de flujo de parámetros."""
    STATUS_WORD1 = 6
    """Status bits."""
    SOFTWARE_VERSION = 7
    """Versión de firmware del controlador."""
    ID = 8
    """ID del controlador."""
    STATUS_WORD2 = 9
    """Status bits."""
    STATUS_WORD3 = 10
    """Status bits."""
    IR = 11
    """Ratio integral."""
    DT = 12
    """Tiempo de derivación."""
    PB = 13
    """Banda proporcional."""
    PRTB = 14
    """Time base for the ramp and soak programs."""
    CT = 15
    """Tiempo de ciclo."""
    FREQ = 16
    """Frecuencia principal."""
    HYST = 17
    """Control de histéresis."""
    FLTR = 18
    """Ganancia del filtro digital para la variable de proceso."""
    OULL = 19
    """Límite inferior de potencia de la salida."""
    OUHL = 20
    """Límite superior de potencia de la salida."""
    SERIAL_NUMBER_HIGH = 23
    """Serial number high."""
    SERIAL_NUMBER_LOW = 24
    """Serial number low."""
    SV = 25
    """Control setpoint."""
    SPLL = 26
    """Límite inferior del setpoin."""
    SPHL = 27
    """Límite superior del setpoint."""
    OFFS = 29
    """Offset para la variable de proceso."""
    DPPO = 30
    """Posición del punto decimal para la variable de proceso."""
    SPA1 = 31
    """Setpoint de la alarma 1."""
    SPA2 = 32
    """Setpoint de la alarma 2."""
    SPA3 = 33
    """Setpoint de la alarma 3."""
    SPA4 = 34
    """Setpoint de la alarma 4."""
    FNA1 = 35
    """Función de alarma 1."""
    FNA2 = 36
    """Función de alarma 2."""
    FNA3 = 37
    """Función de alarma 3."""
    FNA4 = 38
    """Función de alarma 4."""
    HYA1 = 39
    """Histéresis de alarma 1."""
    HYA2 = 40
    """Histéresis de alarma 2."""
    HYA3 = 41
    """Histéresis de alarma 3."""
    HYA4 = 42
    """Histéresis de alarma 4."""
    TYPE = 43
    """Tipo de la variable de proceso."""
    ADDR = 44
    """Dirección de comunicación con el esclavo."""
    BAUD = 45
    """Tasa de comunicación."""
    AUTO = 46
    """Modo de control."""
    RUN = 47
    """Habilita el control."""
    ACT = 48
    """Acción de control."""
    ATUN = 49
    """Auto tune habilitado."""
    BLA1 = 50
    """Habilita encendido de alarma 1."""
    BLA2 = 51
    """Habilita encendido de alarma 2."""
    BLA3 = 52
    """Habilita encendido de alarma 3."""
    BLA4 = 53
    """Habilita encendido de alarma 4."""
    KEY = 54
    """Presionado de teclas remoto."""
    RSLL = 55
    """Límite inferior del SP remoto."""
    RSHL = 56
    """Límite superior del SP remoto."""
    IO1 = 57
    """Funciones I/O."""
    IO2 = 58
    """Funciones I/O."""
    IO3 = 59
    """Funciones I/O."""
    IO4 = 60
    """Funciones I/O."""
    IO5 = 61
    """Funciones I/O."""
    A1T1 = 62
    """Alarma 1 tiempo 1."""
    A1T2 = 63
    """Alarma 1 tiempo 2."""
    A2T1 = 64
    """Alarma 2 tiempo 1."""
    A2T2 = 65
    """Alarma 2 tiempo 2."""
    SFST = 66
    """Tiempo para el soft start."""
    UNIT = 67
    """Unidad de la temperatura."""
    BIAS = 68
    """Bias."""
    RS_SEGMENT = 70
    """Ramp and Soak segment being executed."""
    PRN = 71
    """Segmento RS a ser visto o editado."""
    EPR = 72
    """Segmento RS a ser ejecutado."""
    REMAINING_TIME_RS = 73
    """Tiempo restante del segmento RS."""
    SQRT = 74
    """Raíz cuadrada de una entrada lineal."""
    CALIBRATION_PV_LOW = 75
    """Ingrese el valor de entrada bajo actualmente aplicado en la entrada
    PV para fines de calibración."""
    CALIBRATION_PV_HIGH = 76
    """Ingrese el valor de entrada alto actualmente aplicado en la entrada
    PV para fines de calibración."""
    CALIBRATION_REMOTE_SP_LOW = 77
    """Ingrese el valor de entrada bajo actualmente aplicado en la entrada
    de setpoint remoto para fines de calibración."""
    CALIBRATION_REMOTE_SP_HIGH = 78
    """Ingrese el valor de entrada alto actualmente aplicado en la entrada
    de setpoint remoto para fines de calibración."""
    RTLL = 79
    """Límite bajo de retransmisión."""
    RTLH = 80
    """Límite alto de retransmisión."""
    FLSH = 81
    """Habilita el parpadeo del display superior como una función de la
    alarma seleccionada."""
    A3T1 = 82
    """Alarma 3 tiempo 1."""
    A3T2 = 83
    """Alarma 3 tiempo 2."""
    A4T1 = 84
    """Alarma 4 tiempo 1."""
    A4T2 = 85
    """Alarma 4 tiempo 2."""
    RSTR = 86
    """Restaura a la calibración original."""
    PASS = 87
    """Permite definir una nueva contraseña de acceso."""
    PROT = 88
    """Nivel de protección de la contraseña."""
    PRTY = 89
    """Paridad del serial de comunicación."""
    ERSP = 98
    """Habilita el setpoint remoto."""
    PEVENT = {
        f'{prog_num}': {
            f'{step_num}': register_num for register_num in range(100, 279 + 1)
            for step_num in range(1, 9 + 1)
        } for prog_num in range(1, 20 + 1)
    }
    """Evento del segmento de programa PEVENT[PROG_NUM][PROG_SEGMENT]."""
    PTIME = {
        f'{prog_num}': {
            f'{step_num}': register_num for register_num in range(320, 499 + 1)
            for step_num in range(1, 9 + 1)
        } for prog_num in range(1, 20 + 1)
    }
    """Tiempos de programa PTIME[PROG_NUM][PROG_SEGMENT]."""
    PSETPOINT = {
        f'{prog_num}': {
            f'{step_num}': register_num for register_num in range(500, 699 + 1)
            for step_num in range(1, 9 + 1)
        } for prog_num in range(1, 20 + 1)
    }
    """Tiempos de programa PSETPOINT[PROG_NUM][PROG_SEGMENT]."""
