# Introducción

Conector de Python para hablar con los controladores [Novus](https://www.novusautomation.com/site/).

## Información

El módulo ofrece una capa de administración de conexión para conectarse por puerto serie o por red a los controladores.

## Licencia

El proyecto fue realizado bajo la licencia GPL-3.
